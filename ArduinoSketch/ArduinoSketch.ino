static const uint8_t anaPins[] = {A0, A1, A2, A3, A4, A5};

void setup() {
  Serial.begin(9600);
  // Sets up the pins to output and inits the pins to LOW.
  for (int led = 2; led < 13; led++) {
    pinMode(led, OUTPUT);
    digitalWrite(led, LOW);
  }

  for (int led = 0; led < 4; led++) {
    pinMode(anaPins[led], OUTPUT);
    digitalWrite(anaPins[led], LOW);
  }
}

void loop() {
  // Picks 6 random pin and a random value every 1/20 of a second

  for (int i = 0; i < 6; i++) {
    // Picks what type of pin to use
    int pickPin = random(2);
    int pin = 0;
    int value = random(2); // HIGH or LOW

    if (pickPin == 0) {
      pin = random(5);
      digitalWrite(anaPins[pin], (value == 0) ? LOW : HIGH);
    }
    else {
      pin = random(2, 14);
      digitalWrite(pin, (value == 0) ? LOW : HIGH);
    }
  }

  delay(100);
}
